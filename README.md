# README #

LLVM IR profiling

### Basics ###

This is a LLVM Optimization pass that analyzes and modifies LLVM IR code. Also included is an example on how to use and run the profiler.

### Requirements ###

An installation of LLVM-3.7 on the machine

### The Optimization pass ###

* The optimization pass iterates through code in an ordered fashion and counts the relevant LLVM instructions and tries to detect the type that is being operated on. These counts are then saved to table structures.

* After statically analyzing, the pass proceeds to instrument code on a basic-block level. The instrumentation is simply inserting counters that will be triggered and incremented when execution enters each basic block.

To run the pass, do: ```opt-3.7 -load ./countpass.so -opcount -S -o OUTPUT.ll FILE.ll```. This will output both the instrumented code to OUTPUT.ll and the analysis log to stderr.

### Running the instrumented code ###

The instrumented code can be run with the LLVM JIT compiler, ```lli```. Just do: ```lli-3.7 -load ./bbdump.so FILE.ll```.

Note that you must load the bbdump.so shared object, which contains the function that actually calculates the total execution counts and dumps it to stderr when the code finishes executing.

### How to run ###

* Compile the profiler by simply running ```make``` at the root directory.
* Compile the code to be analyzed to LLVM IR (assuming a monolithic program for simplicity):
  ```clang-3.7 -S -emit-llvm FILE.c```
* Run ```./run_test.sh FILE.ll``` or run optimization pass and instrumented code separately (look at run_test.sh source)

### Compiling and running more complex code ###

To do this, one must proceed as follows:

* Compile each source file to LLVM IR using clang
* Use llvm-link: ```llvm-link-3.7 -S -o OUTPUT.ll FILE1.ll FILE2.ll ...```
* Use the resulting file to feed the pass

Note: If the program calls from external libraries, one *MUST* load these libraries when running ```lli```: ```lli-3.7 -load /path/to/LIBRARY.so```

### Disabling type backtracking ###

By default the pass performs type backtracking on logic/arithmetic and compare instructions (binary operations), to determine the original type that is more close to a C-type. This is because the compiled LLVM code will have sign/zero extensions on values loaded, and the binary operations will be performed on different types than the original memory region that holds the variable's value. Truncate instructions also appear sometimes when the calculated value is stored back.

It is still not very clear if this is the best way to go, so there is the option of disabling the type backtracking at runtime. It is simple to do so: the only requirement is invoke opt with the ```-no-backtrack``` option:

```opt-3.7 -load ./countpass.so -opcount -no-backtrack -S -o OUTPUT.ll FILE.ll```

*NOTE*: be aware that if using backtracking, counting of the sign/zero extend, truncate and int<->float cast instructions makes no sense, because they are already being embedded into the backtracked type.

### Other options ###

You can instruct the instrumented code to automatically save the basic block execution counts to a JSON file by using the ```-save-counts``` option:

```opt-3.7 -load ./countpass.so -opcount -save-counts PREFIX```

When the instrumented code is run, it will save the counts to that file, producing something like this example, for the basic block run count:

```
{
    "0": "19",
    "1": "10",
    "2": "9",
    "3": "18"
}
```

This gets saved as PREFIX.bb.json. The actual operation counts gets saved to PREFIX.prof.json.

## Dynamic dimensionality reduction ##

Under the folder *dreduce* there is a python tool that can take the output from the profiled application execution (operator counts) and dynamically perform dimensionality reduction by means of a configuration file that will instruct the tool on how to proceed. A brief example:

```
{"DimReduceCfg" : [
    {"ReduceRegion" : {
        "ReduceTypes" : ["i8", "i16", "i32", "i64"],
        "ReduceTo" : "int",
        "Scope" : "all"
    }}
]}
```

which reduces all integer types to a single type named *int* for all operators. See examples *coarse.json* and *example.json* in the subfolder.

### How to run the tool ###

Simply:

```
python dreduce.py -c CONFIFILE.json INPUTFILE.prof
```