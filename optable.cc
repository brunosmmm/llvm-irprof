/*! \file optable.cc
    \brief Operation Table for storing profiling information
    \author Bruno Morais <brunosmmm@gmail.com>
*/
#include "optable.hh"
#include <iostream>
#include <algorithm>

namespace IRProfiler
{

    OpTable::OpTable(bool extendedOperators)
        : extendedOpValid (extendedOperators)
    {
        //create entries
        for(OperatorType type : operatorTypes)
        {
            OpTableLine * newLine;

            newLine = new OpTableLine;

            //get supported types for this operator
            const std::set<OperandType> supportedTypes = operatorTypeSupport.at(type);

            for (OperandType opndType : supportedTypes)
            {
                newLine->insert(std::pair<OperandType, unsigned int>(opndType,
                                                                     0));
            }

            theTable.insert(std::pair<OperatorType, OpTableLine*>(type,
                                                                  newLine));
        }
    }

    void OpTable::IncrementValue(OperandType opndType, OperatorType opType)
    {
        OpTableLine * theLine;
        unsigned int * theTypeCount;

        if ((opndType == OperandType::UNKNOWN) ||
            (opType == OperatorType::UNKNOWN))
        {
            //error
            return;
        }

        //go into map and increment
        //go into operator
        theLine = theTable.at(opType);

        //check if type is supported by this operator
        if (operatorTypeSupport.at(opType).find(opndType) == operatorTypeSupport.at(opType).end())
        {
            return;
        }

        //go into operand type
        theTypeCount = &(theLine->at(opndType));
        *theTypeCount = *theTypeCount+1;
    }

    void OpTable::SetValue(OperandType opndType, OperatorType opType, unsigned int value)
    {
        OpTableLine * theLine;
        unsigned int * theTypeCount;

        if ((opndType == OperandType::UNKNOWN) ||
            (opType == OperatorType::UNKNOWN))
        {
            //error
            return;
        }

        //go into map and increment
        //go into operator
        theLine = theTable.at(opType);

                //check if type is supported by this operator
        if (operatorTypeSupport.at(opType).find(opndType) == operatorTypeSupport.at(opType).end())
        {
            return;
        }

        //go into operand type
        theTypeCount = &(theLine->at(opndType));
        *theTypeCount = value;
    }

    unsigned int OpTable::GetCount(OperandType opndType, OperatorType opType)
    {
        OpTableLine * theLine;

        if ((opndType == OperandType::UNKNOWN) ||
            (opType == OperatorType::UNKNOWN))
        {
            //error
            return 0;
        }

        //check if type is supported by this operator
        if (operatorTypeSupport.at(opType).find(opndType) == operatorTypeSupport.at(opType).end())
        {
            return 0;
        }

        theLine = theTable.at(opType);
        return theLine->at(opndType);
    }

    bool OpTable::isEmpty(void)
    {
        //iterate through table
        for (auto &line : theTable)
        {
            for(auto &item : *(line.second))
            {
                if (item.second)
                {
                    return false;
                }
            }
        }

        return true;
    }

    void OpTable::constantMultiply(unsigned int c)
    {
        //iterate through table
        for (auto &line : theTable)
        {
            for (auto &item : *(line.second))
            {
                item.second *= c;
            }
        }
    }

    unsigned int * OpTable::dumpTable(void)
    {
        unsigned int * serialTable;
        unsigned int i;

        //allocate serial table
        serialTable = new unsigned int[getTableSize()];

        //iterate through table
        i = 0;
        for (auto &line : theTable)
        {
            for (auto &item: *(line.second))
            {
                serialTable[i] = item.second;
                i++;
            }
        }

        return serialTable;
    }

    void OpTable::populate(unsigned int * data)
    {
        unsigned int i = 0;
        for (auto &line : theTable)
        {
            for (auto &item : *(line.second))
            {
                item.second = data[i++];
            }
        }
    }

    void OpTable::print(std::ostream &output, char separator)
    {
        for (OperatorType type : operatorTypes)
        {
            const std::set<OperandType> supportedTypes = operatorTypeSupport.at(type);

            //don't print extended operators if not available
            if ((!extendedOpValid) && (ExtendedOpList.find(type) != ExtendedOpList.end()))
            {
                continue;
            }

            for (OperandType opndType : supportedTypes)
            {
                output << operatorNames.at(type) << separator
                       << operandNames.at(opndType) << separator
                       << GetCount(opndType, type) << std::endl;
            }

            output << std::endl;
        }
    }

    miniJSON::JSONObject * OpTable::getJSON(void)
    {
        miniJSON::JSONObject * tableJSON = new miniJSON::JSONObject;

        for (OperatorType type : operatorTypes)
        {
            const std::set<OperandType> supportedTypes = operatorTypeSupport.at(type);

            if ((!extendedOpValid) && (ExtendedOpList.find(type) != ExtendedOpList.end()))
            {
                continue;
            }

            miniJSON::JSONObject * operatorCounts = new miniJSON::JSONObject;
            miniJSON::JSONField * operatorContainer = new miniJSON::JSONField(operatorNames.at(type), operatorCounts);

            for (OperandType opndType : supportedTypes)
            {
                miniJSON::JSONField * operandCount = new miniJSON::JSONField(operandNames.at(opndType), GetCount(opndType, type));

                operatorCounts->AddField(operandCount);

            }

            tableJSON->AddField(operatorContainer);
        }

        return tableJSON;
    }

    void OpTable::printJSON(std::ostream & output)
    {
        miniJSON::JSONWriter tableOut;
        miniJSON::JSONObject * tableJSON = getJSON();

        tableOut.SetRoot(tableJSON);
        tableOut.Dump(output);

        delete tableJSON;
    }

    OpTable operator*(const OpTable& table, unsigned int c)
    {
        OpTable result(table.extendedOpValid);

        for(OperatorType type : operatorTypes)
        {

            const std::set<OperandType> supportedTypes = operatorTypeSupport.at(type);

            for (OperandType opndType : supportedTypes)
            {
                result.theTable.at(type)->at(opndType) = table.theTable.at(type)->at(opndType)*c;
            }
        }

        return result;
    }

    OpTable operator*(unsigned int c, const OpTable& table)
    {
        OpTable result(table.extendedOpValid);

        for(OperatorType type : operatorTypes)
        {

            const std::set<OperandType> supportedTypes = operatorTypeSupport.at(type);

            for (OperandType opndType : supportedTypes)
            {
                result.theTable.at(type)->at(opndType) = table.theTable.at(type)->at(opndType)*c;
            }
        }

        return result;
    }

    //element wise multiplication
    OpTable operator*(const OpTable& table1, const OpTable& table2)
    {
        OpTable result(table1.extendedOpValid || table2.extendedOpValid);
        for(OperatorType type : operatorTypes)
        {

            const std::set<OperandType> supportedTypes = operatorTypeSupport.at(type);

            for (OperandType opndType : supportedTypes)
            {
                result.theTable.at(type)->at(opndType) = table1.theTable.at(type)->at(opndType) *
                    table2.theTable.at(type)->at(opndType);
            }
        }

        return result;
    }

    OpTable operator+(const OpTable& table1, const OpTable& table2)
    {
        OpTable result(table1.extendedOpValid || table2.extendedOpValid);
        for (OperatorType type : operatorTypes)
        {
            const std::set<OperandType> supportedTypes = operatorTypeSupport.at(type);
            for (OperandType opndType : supportedTypes)
            {
                result.theTable.at(type)->at(opndType) = table1.theTable.at(type)->at(opndType) +
                    table2.theTable.at(type)->at(opndType);
            }
        }

        return result;
    }

    OperatorType LLVMOpcodeToOperatorType(unsigned opcode, bool someCondition)
    {
        switch(opcode)
        {
        case llvm::Instruction::Add:
        case llvm::Instruction::FAdd:
            return OperatorType::ADD;
        case llvm::Instruction::Sub:
        case llvm::Instruction::FSub:
            return OperatorType::SUB;
        case llvm::Instruction::Mul:
        case llvm::Instruction::FMul:
            return OperatorType::MUL;
        case llvm::Instruction::UDiv:
        case llvm::Instruction::SDiv:
        case llvm::Instruction::FDiv:
            return OperatorType::DIV;
        case llvm::Instruction::SRem:
        case llvm::Instruction::URem:
        case llvm::Instruction::FRem:
            return OperatorType::REM;
        case llvm::Instruction::Load:
            return OperatorType::LOAD;
        case llvm::Instruction::Store:
            return OperatorType::STORE;
        case llvm::Instruction::GetElementPtr:
            return OperatorType::ARRAY;
        case llvm::Instruction::Br:
            if (someCondition)
            {
                return OperatorType::CBRANCH;
            }
            else
            {
                return OperatorType::UBRANCH;
            }
        case llvm::Instruction::And:
            return OperatorType::AND;
        case llvm::Instruction::Or:
            return OperatorType::OR;
        case llvm::Instruction::Xor:
            return OperatorType::XOR;
        case llvm::Instruction::Shl:
            return OperatorType::SHL;
        case llvm::Instruction::AShr:
        case llvm::Instruction::LShr:
            return OperatorType::SHR;
        case llvm::Instruction::Call:
            return OperatorType::FCALL;
        case llvm::Instruction::Ret:
            return OperatorType::FRET;
        case llvm::Instruction::ZExt:
            return OperatorType::ZEXT;
        case llvm::Instruction::SExt:
            return OperatorType::SEXT;
        case llvm::Instruction::FPExt:
            return OperatorType::FPEXT;
        case llvm::Instruction::Trunc:
        case llvm::Instruction::FPTrunc:
            return OperatorType::TRUNC;
        case llvm::Instruction::SIToFP:
        case llvm::Instruction::UIToFP:
            return OperatorType::ITOF;
        case llvm::Instruction::FPToUI:
        case llvm::Instruction::FPToSI:
            return OperatorType::FTOI;
        default:
            break;
        }

        //ERROR
        std::cerr << "ERROR: unknown opcode: " << opcode << std::endl;
        return OperatorType::UNKNOWN;
    }

    OperatorType LLVMCmpPredicateToOperatorType(llvm::CmpInst::Predicate pred)
    {

        switch(pred)
        {
        case llvm::CmpInst::Predicate::FCMP_UEQ:
        case llvm::CmpInst::Predicate::FCMP_OEQ:
        case llvm::CmpInst::Predicate::ICMP_EQ:
            return OperatorType::CMPEQ;
        case llvm::CmpInst::Predicate::FCMP_ONE:
        case llvm::CmpInst::Predicate::FCMP_UNE:
        case llvm::CmpInst::Predicate::ICMP_NE:
            return OperatorType::CMPNE;
        case llvm::CmpInst::Predicate::FCMP_OGT:
        case llvm::CmpInst::Predicate::FCMP_UGT:
        case llvm::CmpInst::Predicate::ICMP_SGT:
        case llvm::CmpInst::Predicate::ICMP_UGT:
            return OperatorType::CMPGT;
        case llvm::CmpInst::Predicate::FCMP_OLT:
        case llvm::CmpInst::Predicate::FCMP_ULT:
        case llvm::CmpInst::Predicate::ICMP_SLT:
        case llvm::CmpInst::Predicate::ICMP_ULT:
            return OperatorType::CMPLT;
        case llvm::CmpInst::Predicate::FCMP_OLE:
        case llvm::CmpInst::Predicate::FCMP_ULE:
        case llvm::CmpInst::Predicate::ICMP_SLE:
        case llvm::CmpInst::Predicate::ICMP_ULE:
            return OperatorType::CMPLE;
        case llvm::CmpInst::Predicate::FCMP_OGE:
        case llvm::CmpInst::Predicate::FCMP_UGE:
        case llvm::CmpInst::Predicate::ICMP_SGE:
        case llvm::CmpInst::Predicate::ICMP_UGE:
            return OperatorType::CMPGE;
        default:
            break;
        }

        std::cerr << "ERROR: unknown predicate: " << pred << std::endl;
        return OperatorType::UNKNOWN;
    }

    OperandType LLVMTypeToOperandType(llvm::Type * llvmType)
    {
        if (!llvmType)
        {
            //error
            std::cerr << "ERROR: type error" << std::endl;
            return OperandType::UNKNOWN;
        }

        if (llvmType->isFloatTy())
        {
            return OperandType::FLOAT32;
        }

        if (llvmType->isDoubleTy())
        {
            return OperandType::FLOAT64;
        }

        //equivalent to a boolean type, however
        //minium data size is often byte-sized,
        //treat as an INT8
        if (llvmType->isIntegerTy(1))
        {
            return OperandType::INT8;
        }

        if (llvmType->isIntegerTy(8))
        {
            return OperandType::INT8;
        }

        if (llvmType->isIntegerTy(16))
        {
            return OperandType::INT16;
        }

        if (llvmType->isIntegerTy(32))
        {
            return OperandType::INT32;
        }

        if (llvmType->isIntegerTy(64))
        {
            return OperandType::INT64;
        }

        if (llvmType->isPointerTy())
        {
            return OperandType::POINTER;
        }

        std::cerr << "ERROR: unknown type: ";
        llvmType->dump();

        return OperandType::UNKNOWN;
    }

    unsigned int OpTable::getTableSize(void)
    {
        unsigned int totalSize = 0;
        //calculate table size
        for (OperatorType opType: operatorTypes)
        {
            totalSize += operatorTypeSupport.at(opType).size();
        }

        return totalSize;
    }
    
}
