#include "minijson.hh"
#include <sstream>

namespace miniJSON
{

    JSONField::JSONField(std::string name, std::string value)
        : objectContents (NULL),
          intContents (NULL),
          identifier (name)
    {
        strContents = new std::string(value);
    }

    JSONField::JSONField(std::string name, int value)
        : objectContents (NULL),
          strContents (NULL),
          identifier (name)
    {
        intContents = new int(value);
    }

    JSONField::JSONField(std::string name, JSONObject * obj)
        : objectContents(obj),
          intContents(NULL),
          strContents(NULL),
          identifier (name)
    {

    }

    void JSONWriter::SetRoot(JSONObject * rootObject)
    {
        root = rootObject;
    }

    void JSONObject::AddField(JSONField * field)
    {
        fields.push_back(field);
    }

    void JSONField::Dump(std::ostream &out, unsigned int currentIndent)
    {
        std::string currentIndentation = getIndentationStr(currentIndent);

        out << currentIndentation << "\"" << identifier << "\": ";

        if (intContents)
        {
            out << "\"" << *intContents << "\"";
            return;
        }

        if (strContents)
        {
            out << "\"" << *strContents << "\"";
            return;
        }

        if (objectContents)
        {
            //out << std::endl;
            objectContents->Dump(out, currentIndent);
            return;
        }
    }

    void JSONObject::Dump(std::ostream &out, unsigned int currentIndent)
    {
        std::string currentIndentation = getIndentationStr(currentIndent);

        out << "{" << std::endl;

        unsigned int outputtedFields = 0;
        for (auto &field : fields)
        {
            field->Dump(out, currentIndent+1);

            if (outputtedFields < fields.size() - 1)
            {
                out << "," ;
            }

            outputtedFields++;

            out << std::endl;
        }

        out << currentIndentation << "}";
        
    }

    void JSONWriter::Dump(std::ostream &out)
    {
        root->Dump(out, 0);

        out << std::endl;
    }

    //helper function
    std::string getIndentationStr(unsigned int currentIndent)
    {
        std::string result;

        while (currentIndent--)
        {
            result += indentStr;
        }

        return result;
    }

}
