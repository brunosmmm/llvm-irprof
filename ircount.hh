/*! \file ircount.hh
    \brief Hierarchical storage of profiling data
    \author Bruno Morais <brunosmmm@gmail.com>
*/
#ifndef IRCOUNT_H
#define IRCOUNT_H

#include <map>
#include <string>
#include <list>
#include <vector>
#include "optable.hh"
#include "llvm/IR/BasicBlock.h"

namespace IRProfiler
{
    /*! Lower hierarchical level for data storage, basic block level */
    class IRBasicBlockStats
    {
    private:
        OpTable * opTable;
        llvm::BasicBlock * bb;
    public:
        IRBasicBlockStats(OpTable* table, llvm::BasicBlock * bb);

        /*! Access the OpTable stored
          \returns a pointer to the OpTable
        */
        OpTable* accessTable(void) const { return opTable; }

        /*! Get the LLVM basic block related to this storage
          \returns pointer to the basic block
        */
        llvm::BasicBlock* getBasicBlock(void) { return bb; }
    };

    /*! Function-level storage, stores function information and all the function's basic blocks */
    class IRFunctionStats
    {
    private:
        /*! List of basic block storage units */
        std::list<IRBasicBlockStats> bbList;
        unsigned int bbCount;
        std::string funcName;

    public:
        IRFunctionStats(std::string funcName, unsigned int bbCount=0);

        /*! Add a basic block to this storage
          \param bbStats the data storage element
        */
        void addBasicBlockStats(IRBasicBlockStats bbStats) { bbList.push_back(bbStats); }

        /*! Get function name
          \return function name
        */
        std::string getName(void) { return funcName; }

        /*! Get number of basic blocks in function
          \return the count
        */
        unsigned int getBBCount(void) { return bbCount; }

        /*! Set number of basic blocks in function
          \param bbCount the number of bbs
        */
        void setBBCount(unsigned int bbCount) { bbCount = bbCount; }

        /*! Access a basic block storage element
          \param bb pointer to a LLVM basic block to index into
          \return the storage element
        */
        IRBasicBlockStats * getBasicBlockStats(llvm::BasicBlock* bb);

        /*! Returns the sum of all OpTables
          \return the overall table
        */
        OpTable getOverallOpTable(std::vector<unsigned int> bbCounts);

    };

    /*! Highest hierarchical storage level */
    class IRProgramStats
    {
    private:
        std::list<IRFunctionStats> functionStats;
    public:
        /*! Add a function storage element
           \param funcStats the element
        */
        void addFunctionStats(IRFunctionStats funcStats) { functionStats.push_back(funcStats); }

        /*! Get a function storage element by name
          \param funcName the function name
        */
        IRFunctionStats * getFunctionStats(std::string funcName);

        /*! Get sum of all OpTables
          \return the overall table
        */
        OpTable getOverallOpTable(std::vector<unsigned int> bbCounts);
    };
}


#endif /* IRCOUNT_H */
