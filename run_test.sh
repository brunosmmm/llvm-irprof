#!/bin/sh
PASSLIB=./countpass.so
PASSNAME=opcount
BBDUMPLIB=bbdump.so
TESTFILE=testpass.bc

if [ $# -eq 0 ]
then
    echo "Error, no input file"
    exit 1
fi

#check file
if [ ! -f $1 ]
then
    echo "Error, input file does not exist"
    exit 1
fi

#make some stuff
if [ ! -f $BBDUMPFILE ]
then
    make $BBDUMPLIB
fi

#call LLVM (with instrumentated code)
#llvm-link $BBDUMPFILE $1 -S | opt -load $PASSLIB -$PASSNAME | lli
opt-3.7 -load $PASSLIB -$PASSNAME -save-counts $1 -o $TESTFILE -S $1 2> $1.stat
echo "INFO: running instrumented code, output below:"
lli-3.7 -load ./$BBDUMPLIB $TESTFILE
