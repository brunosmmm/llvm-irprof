## Operator and operand type definitions
## @file opdefs.py
## @author Bruno Morais <brunosmmm@gmail.com>

OPERATOR_BY_NAME = ['ADD',
                    'SUB',
                    'MUL',
                    'DIV',
                    'REM',
                    'LOAD',
                    'STORE',
                    'ARRAY',
                    'UBR',
                    'CBR',
                    'AND',
                    'OR',
                    'XOR',
                    'SHL',
                    'SHR',
                    'FCALL',
                    'FRET',
                    'CMPEQ',
                    'CMPNE',
                    'CMPGT',
                    'CMPLT',
                    'CMPLE',
                    'CMPGE',
                    'SEXT',
                    'ZEXT',
                    'FPEXT',
                    'TRUNC',
                    'ITOF',
                    'FTOI']

OPERAND_BY_NAME = ['INT8',
                   'INT16',
                   'INT32',
                   'INT64',
                   'FLOAT',
                   'DOUBLE',
                   'POINTER',
                   'VOID']

OPERAND_INT_TYPES = ['INT8',
                     'INT16',
                     'INT32',
                     'INT64']

OPERAND_FP_TYPES = ['FLOAT',
                    'DOUBLE']

OPERAND_POINTER_TYPES = ['POINTER']
OPERAND_VOID_TYPES = ['VOID']

OPERATOR_TYPE_SUPPORT = {'ADD' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'SUB' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'MUL' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'DIV' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'REM' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'AND' : OPERAND_INT_TYPES + OPERAND_POINTER_TYPES,
                         'OR' : OPERAND_INT_TYPES + OPERAND_POINTER_TYPES,
                         'XOR' : OPERAND_INT_TYPES + OPERAND_POINTER_TYPES,
                         'SHR' : OPERAND_INT_TYPES + OPERAND_POINTER_TYPES,
                         'SHL' : OPERAND_INT_TYPES + OPERAND_POINTER_TYPES,
                         'LOAD' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'STORE' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'CMPEQ' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'CMPNE' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'CMPGT' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'CMPLT' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'CMPGE' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'CMPLE' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'ARRAY' : OPERAND_VOID_TYPES,
                         'FCALL' : OPERAND_VOID_TYPES,
                         'FRET' : OPERAND_VOID_TYPES,
                         'UBR' : OPERAND_VOID_TYPES,
                         'CBR' : OPERAND_VOID_TYPES,
                         'SEXT' : OPERAND_INT_TYPES + OPERAND_POINTER_TYPES,
                         'ZEXT' : OPERAND_INT_TYPES + OPERAND_POINTER_TYPES,
                         'FPEXT' : OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'TRUNC' : OPERAND_INT_TYPES + OPERAND_FP_TYPES + OPERAND_POINTER_TYPES,
                         'ITOF' : OPERAND_INT_TYPES + OPERAND_POINTER_TYPES,
                         'FTOI' : OPERAND_FP_TYPES + OPERAND_POINTER_TYPES}
