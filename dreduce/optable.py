## Operation Tables
## @file optable.py
## @author Bruno Morais <brunosmmm@gmail.com>

from opdefs import *
import re
import simplejson

class OpTable(object):
    """Operation table storage class"""
    def __init__(self, operator_list, type_support=OPERATOR_TYPE_SUPPORT):

        #create table as dictionaries
        self.entries = dict([(x, dict([(y,0) for y in type_support[x]])) for x in operator_list])

    def set_entry_value(self, operator, operand, value):
        """Set the value of a table entry"""

        #error checking
        if operator not in self.entries:
            raise KeyError('invalid operator: {}'.format(operator))

        if operand not in self.entries[operator]:
            raise KeyError('invalid operand for operator {}: {}'.format(operator,
                                                                        operand))
        if value < 0:
            raise ValueError('invalid value: {}'.format(value))

        #set value
        self.entries[operator][operand] = value

    def get_table_size(self):
        """Get total table size in element count"""
        total_size = 0
        for operator in self.entries.values():
            total_size += len(operator)

        return total_size

    def read_table_from_json(self, json):

        for operator, operands in json.iteritems():

            if operator not in OPERATOR_BY_NAME:
                raise TypeError('unknown operator type: {}'.format(operator))

            for operand, count in operands.iteritems():

                if operand not in OPERAND_BY_NAME:
                    raise TypeError('unknown operand type: {}'.format(operand))

                if count < 0:
                    raise ValueError('invalid count: {}'.format(count))

                self.set_entry_value(operator,
                                     operand, count)

    def read_table_from_text(self, lines, separator=' '):
        """Reads from a .prof file, DEPRECATED"""
        for line in lines:

            m = re.match(r'([a-zA-Z]+){0}([a-zA-Z0-9]+){0}([0-9]+).*'.format(separator),
                         line)

            if m != None:
                operator = m.group(1)
                operand = m.group(2)
                count = int(m.group(3))

                if operator not in OPERATOR_BY_NAME:
                    raise TypeError('unknown operator type: {}'.format(operator))

                if operand not in OPERAND_BY_NAME:
                    raise TypeError('unknown operand type: {}'.format(operand))

                if count < 0:
                    raise ValueError('invalid count: {}'.format(count))

                self.set_entry_value(OPERATOR_BY_NAME[operator],
                                     OPERAND_BY_NAME[operand], count)

            else:
                print "WARNING: invalid line detected: {}".format(line)

    def dump_table_contents(self, separator=' '):
        """Formats table contents for output"""
        lines = []
        for operator, operands in self.entries.iteritems():
            for operand, count in operands.iteritems():
                try:
                    op_name = get_operator_name(operator)
                except KeyError:
                    op_name = str(operator)
                try:
                    opnd_name = get_operand_name(operand)
                except KeyError:
                    opnd_name = str(operand)

                lines.append("{}{sep}{}{sep}{}\n".format(op_name,
                                                         opnd_name, count,
                                                         sep=separator))

        return ''.join(lines)

    def dump_json(self):

        json = simplejson.dumps(self.entries, indent=4*' ')

        return json


    def __repr__(self):
        return "OpTable: {} operators, {} entries".format(len(self.entries),
                                                          self.get_table_size())
