## Dimensionality reduction for profiling results
## @file dreduce.py
## @author Bruno Morais <brunosmmm@gmail.com>

import re
import argparse
from opdefs import *
from optable import OpTable
from reducecfg import DimReduceConfiguration
import simplejson

class ProfileData(object):
    """content to hold otpable data"""
    def __init__(self):
        #default table with all possible operator combinations
        self.op_table = OpTable(OPERATOR_BY_NAME)

    def load_from_file(self, filename):
        """load table from profiler output"""
        try:
            f = open(filename, 'r')
            contents = simplejson.loads(f.read())
        except IOError:
            raise

        #fill table
        self.op_table.read_table_from_json(contents)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    #basic program arguments
    parser.add_argument('input', help='input filename')
    parser.add_argument('-o', help='output filename', default=None, type=str)
    parser.add_argument('-c', help='configuration file', type=str)

    args = vars(parser.parse_args())

    #read data
    prof_data = ProfileData()
    prof_data.load_from_file(args['input'])

    if args['c'] != None:
        try:
            dreduce = DimReduceConfiguration(from_file=args['c'])
        except IOError:
            raise

        condensed_data = dreduce.reduce_optable(prof_data.op_table)
        reduce_out = condensed_data.dump_json()
    else:
        #do nothing
        reduce_out = prof_data.op_table.dump_json()

    if args['o'] != None:

        try:
            f = open(args['o'], 'w')
            f.write(reduce_out)
        except IOError:
            raise

    else:
        print reduce_out
