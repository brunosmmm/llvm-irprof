##Dimensionality reduction from configuration files
## @file reducecfg.py
## @author Bruno Morais <brunosmmm@gmail.com>

import simplejson
from opdefs import *
from optable import OpTable

#configuration strings
REDUCE_CFG_OBJECT = 'DimReduceCfg'
REDUCE_REGION_OBJECT = 'ReduceRegion'
REDUCE_TYPES_ARRAY = 'ReduceTypes'
REDUCE_OP_ARRAY = 'ReduceOperators'
REDUCE_TO_OBJECT = 'ReduceTo'
SCOPE_OBJECT = 'Scope'

NATIVE_TYPE_FROM_JSON = {'i8' : 'INT8',
                         'i16' : 'INT16',
                         'i32' : 'INT32',
                         'i64' : 'INT64',
                         'float' : 'FLOAT',
                         'double' : 'DOUBLE',
                         'void' : 'VOID',
                         'ptr' : 'POINTER'}

class DimReduceKinds(object):
    """Reduce operation kinds"""
    TYPE = 0
    OPERATOR = 1

class DimReduceDescriptor(object):
    """Description of a reducing procedure"""
    def __init__(self, kind, reduce_list, reduce_to=None, scope=None):

        self.r_kind = kind
        self.r_list = reduce_list
        self.r_to = reduce_to
        if scope != None:
            self.r_scope = scope
        else:
            self.r_scope = "all"

    @classmethod
    def json_type_to_native(cls, type_val):
        return NATIVE_TYPE_FROM_JSON[type_val]

    @classmethod
    def json_type_list_to_native(cls, type_list):

        return [NATIVE_TYPE_FROM_JSON[x] for x in type_list]

    @classmethod
    def json_op_list_to_native(cls, op_list):

        native_list = []
        for op in op_list:
            if op.upper() not in OPERATOR_BY_NAME:
                raise ValueError('invalid operator specified: {}'.format(op))

            native_list.append(op.upper())

        return native_list

    def get_kind(self):
        return self.r_kind

    def get_reduce_list(self):
        return self.r_list

    def get_reduce_target(self):
        return self.r_to

    def get_scope(self):
        return self.r_scope

    def __repr__(self):
        return "ReduceDescriptor({}): {} -> {}".format("T" if self.r_kind == DimReduceKinds.TYPE else "O",
                                                       self.r_list, self.r_to)

class DimReduceRegion(object):
    """Reduce region representation"""
    def __init__(self):
        self.reduction_list = []

    def add_reduce_descriptor(self, descr):

        if not isinstance(descr, DimReduceDescriptor):
            raise TypeError('invalid reduction descriptor')

        self.reduction_list.append(descr)

class DimReduceConfiguration(object):
    """Configuration interpreter and OpTable reducer"""
    def __init__(self, from_file=None):

        self.regions = []

        if from_file != None:
            self.read_from_file(from_file)

    def read_from_file(self, filename):

        try:
            f = open(filename, 'r')
            json_contents = simplejson.loads(f.read())
        except IOError:
            raise

        if REDUCE_CFG_OBJECT not in json_contents:
            raise IOError('invalid configuration file')

        contents = json_contents[REDUCE_CFG_OBJECT]

        #iterate through elements (list)
        for content in contents:

            new_region = None
            if REDUCE_REGION_OBJECT in content:
                new_region = DimReduceRegion()

                reduce_operators = None
                reduce_types = None
                reduce_to = None
                scope = None
                for key, field in content[REDUCE_REGION_OBJECT].iteritems():
                    if key == REDUCE_TYPES_ARRAY:
                        reduce_types = field
                    elif key == REDUCE_OP_ARRAY:
                        reduce_operators = field
                    elif key == REDUCE_TO_OBJECT:
                        reduce_to = field
                    elif key == SCOPE_OBJECT:
                        scope = field

                if reduce_operators == None and reduce_types == None:
                    raise IOError('malformed configuration file')

                if reduce_operators != None and reduce_types != None:
                    raise IOError('malformed configuration file')

                new_descr = None

                if reduce_types != None:
                    new_descr = DimReduceDescriptor(kind=DimReduceKinds.TYPE,
                                                    reduce_list=DimReduceDescriptor.json_type_list_to_native(reduce_types),
                                                    reduce_to=reduce_to,
                                                    scope=scope)
                elif reduce_operators != None:
                    new_descr = DimReduceDescriptor(kind=DimReduceKinds.OPERATOR,
                                                    reduce_list=DimReduceDescriptor.json_op_list_to_native(reduce_operators),
                                                    reduce_to=reduce_to,
                                                    scope=scope)

                if new_descr:
                    new_region.add_reduce_descriptor(new_descr)

            if new_region:
                self.regions.append(new_region)

    def reduce_optable(self, optable):

        all_operators = set(OPERATOR_BY_NAME)
        all_operands = set(OPERAND_BY_NAME)

        operator_support = dict(OPERATOR_TYPE_SUPPORT)

        #determine which operators and operands will be condensed
        condensed_value_list = []
        for region in self.regions:
            for description in region.reduction_list:
                if description.get_kind() == DimReduceKinds.TYPE:
                    if description.get_scope == "all":
                        all_operands -= set(description.get_reduce_list())
                        all_operands |= set([description.get_reduce_target()])

                    #rebuild operator support list
                    for operator, supported in operator_support.iteritems():
                        #check scope
                        if description.get_scope() != "all" and operator not in description.get_scope():
                            continue
                        sup = set(supported)

                        #check if contains types, so as not to introduce
                        #new types
                        if sup != (sup - set(description.get_reduce_list())):
                            sup |= set([description.get_reduce_target()])
                        else:
                            #nothing to do, doesn't support types
                            continue

                        sup -= set(description.get_reduce_list())
                        operator_support[operator] = list(sup)

                        #condense values
                        removed_operands = set(optable.entries[operator].keys()) & set(description.get_reduce_list())

                        accumulate_counts = 0
                        for operand in removed_operands:
                            accumulate_counts += optable.entries[operator][operand]

                        #create a tuple that describes this value to be inserted later
                        condensed_value_list.append((operator,
                                                     description.get_reduce_target(),
                                                     accumulate_counts))

        #intermediate table
        new_optable = OpTable(list(all_operators), operator_support)

        #apply condensed values
        for operator, operand, value in condensed_value_list:
            new_optable.entries[operator][operand] = value

        #copy other values over
        for operator in optable.entries:
            common_operands = set(optable.entries[operator].keys()) & set(new_optable.entries[operator].keys())
            for operand in common_operands:
                new_optable.entries[operator][operand] = optable.entries[operator][operand]

        #condensing of types done; start condensing of operators
        condensed_value_list = []
        for region in self.regions:
            for description in region.reduction_list:
                if description.get_kind() == DimReduceKinds.OPERATOR:
                    removed_operators = set(description.get_reduce_list())
                    all_operators -= removed_operators
                    all_operators |= set([description.get_reduce_target()])

                    #merge supported types
                    merged_support = set()
                    for operator in removed_operators:
                        merged_support |= set(operator_support[operator])

                    #insert new merged operator
                    operator_support[description.get_reduce_target()] = list(merged_support)

                    accumulate_counts = dict([(x, 0) for x in merged_support])
                    for operator in removed_operators:
                        for operand in operator_support[operator]:
                            accumulate_counts[operand] += new_optable.entries[operator][operand]

                    condensed_value_list.append((description.get_reduce_target(),
                                                 accumulate_counts))

                    #remove old operators
                    for operator in removed_operators:
                        del operator_support[operator]


        #create one more optable
        final_optable = OpTable(list(all_operators), operator_support)

        #apply condensed values
        for operator, values in condensed_value_list:
            for operand, value in values.iteritems():
                final_optable.entries[operator][operand] = value

        #copy other values over
        common_operators = set(new_optable.entries.keys()) & set(final_optable.entries.keys())
        for operator in common_operators:
            final_optable.entries[operator] = dict(new_optable.entries[operator])


        return final_optable
