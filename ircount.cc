/*! \file ircount.cc
    \brief Hierarchical storage of profiling data
    \author Bruno Morais <brunosmmm@gmail.com>
*/
#include "ircount.hh"

namespace IRProfiler
{
    //basic block stats
    IRBasicBlockStats::IRBasicBlockStats(OpTable* table, llvm::BasicBlock* bb)
        : bb (bb),
          opTable(table)
    {
        //empty
    }

    //function stats
    IRFunctionStats::IRFunctionStats(std::string funcName, unsigned int bbCount)
        : bbCount (bbCount),
          funcName (funcName)
    {
        //empty
    }

    IRBasicBlockStats * IRFunctionStats::getBasicBlockStats(llvm::BasicBlock* bb)
    {
        for (std::list<IRBasicBlockStats>::iterator bbit = bbList.begin();
             bbit != bbList.end(); bbit++)
        {
            if (bbit->getBasicBlock() == bb)
            {
                return &(*bbit);
            }
        }

        return NULL;
    }

    //TODO: receive basic block counts and multiply, otherwise makes no sense
    OpTable IRFunctionStats::getOverallOpTable(std::vector<unsigned int> bbCounts)
    {
        OpTable result;
        unsigned int currentBB = 0;

        if (bbCounts.size() != bbList.size())
        {
            //ERROR
            std::cerr << "ERROR: incompatible sizes, can't calculate function totals" << std::endl;
            return result;
        }

        //multiply by bb count and sum
        for (std::list<IRBasicBlockStats>::iterator bbit = bbList.begin();
             bbit != bbList.end(); bbit++)
        {
            OpTable thisBB = *(bbit->accessTable()) * bbCounts[currentBB++];

            result = result + thisBB;
        }

        return result;
    }

    //program stats
    IRFunctionStats * IRProgramStats::getFunctionStats(std::string funcName)
    {
        for (std::list<IRFunctionStats>::iterator func = functionStats.begin();
             func != functionStats.end(); func++)
        {
            if (func->getName() == funcName)
            {
                return &(*func);
            }
        }

        return NULL;
    }

    OpTable IRProgramStats::getOverallOpTable(std::vector<unsigned int> bbCounts)
    {
        OpTable result;
        unsigned int bbOffset = 0;

        for (std::list<IRFunctionStats>::iterator func = functionStats.begin();
             func != functionStats.end(); func++)
        {
            std::vector<unsigned int> funcCounts(bbCounts.begin() + bbOffset,
                                                 bbCounts.begin() + bbOffset + func->getBBCount());

            //get summed function table
            OpTable funcOverall = func->getOverallOpTable(funcCounts);

            bbOffset += func->getBBCount();

            //accumulate values
            result = result + funcOverall;
        }

        return result;
    }
}
