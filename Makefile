CC=g++
CCFLAGS = -fPIC -g -std=c++11
SHARED = -shared
LLVMFLAGS = `llvm-config-3.7 --cppflags`
LLVMLINK = `llvm-config --libs` `llvm-config --ldflags``llvm-config --system-libs`
HEADERS = optable.hh ircount.hh
OBJFILES = countpass.o optable.o ircount.o minijson.o
PASSOPT ?=

all: countpass.so bbdump.so

%.o: %.cc
	$(CC) $(PASSOPT) $(CCFLAGS) $(LLVMFLAGS) -c $<

countpass.so : $(OBJFILES)
	$(CC) $(SHARED) -o $@ $^

bbdump.so: bbdump.o ircount.o optable.o minijson.o
	$(CC) $(CCFLAGS) $(SHARED) -o $@ $^

tests:
	$(MAKE) -C tests

clean:
	rm -f *.o *.so *.bc
