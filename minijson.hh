#ifndef MINIJSON_H
#define MINIJSON_H

#include <iostream>
#include <list>

namespace miniJSON
{
    const std::string indentStr = "    ";

    class JSONField;
    class JSONWriter;

    class JSONObject
    {
    friend class JSONWriter;
    friend class JSONField;
    public:
        JSONObject() {}
        void AddField(JSONField * field);
    private:
        std::list<JSONField*> fields;
        void Dump(std::ostream &out, unsigned int currentIndent);
    };

    class JSONField
    {
    friend class JSONWriter;
    friend class JSONObject;
    public:
        JSONField(std::string name, std::string value);
        JSONField(std::string name, int value);
        JSONField(std::string name, JSONObject * obj);
    private:
        JSONObject * objectContents;
        int * intContents;
        std::string * strContents;

        std::string identifier;

        void Dump(std::ostream &out, unsigned int currentIndent);
    };

    class JSONWriter
    {
    public:
        JSONWriter() {}
        void Dump(std::ostream &out);
        void SetRoot(JSONObject* rootObject);
    private:
        JSONObject* root;
    };

    std::string getIndentationStr(unsigned int currentIndent);
}


#endif /* MINIJSON_H */
