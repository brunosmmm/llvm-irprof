/*! \file optable.hh
    \brief Operation Table class and constant definitions
    \author Bruno Morais <brunosmmm@gmail.com>
*/
#ifndef OPTABLE_H
#define OPTABLE_H

#include <map>
#include <array>
#include <list>
#include <set>
#include <string>
#include <iostream>
#include "minijson.hh"
#include "llvm/IR/Instructions.h"

namespace IRProfiler
{
    /*! Tracked operator types */
    enum class OperatorType { ADD, SUB, MUL, DIV, REM, LOAD, STORE, ARRAY, UBRANCH, CBRANCH,
            AND, OR, XOR, SHL, SHR, FCALL, FRET, CMPEQ, CMPNE, CMPGT, CMPLT, CMPLE, CMPGE,
            SEXT, ZEXT, FPEXT, TRUNC, ITOF, FTOI,
            UNKNOWN };
    /*! Total operator count */
    const unsigned int operatorTypeCount = 29;

    /*! Tracked operand types */
    enum class OperandType { INT8, INT16, INT32, INT64, FLOAT32, FLOAT64, POINTER, VOID, UNKNOWN };
    /*! Total operand (type) count */
    const unsigned int operandTypeCount = 8;
    /*! List of all operator types, for convenience */
    const std::array<OperatorType,operatorTypeCount> operatorTypes = {OperatorType::ADD,
                                                                      OperatorType::SUB,
                                                                      OperatorType::MUL,
                                                                      OperatorType::DIV,
                                                                      OperatorType::REM,
                                                                      OperatorType::LOAD,
                                                                      OperatorType::STORE,
                                                                      OperatorType::ARRAY,
                                                                      OperatorType::UBRANCH,
                                                                      OperatorType::CBRANCH,
                                                                      OperatorType::AND,
                                                                      OperatorType::OR,
                                                                      OperatorType::XOR,
                                                                      OperatorType::SHL,
                                                                      OperatorType::SHR,
                                                                      OperatorType::FCALL,
                                                                      OperatorType::FRET,
                                                                      OperatorType::CMPEQ,
                                                                      OperatorType::CMPNE,
                                                                      OperatorType::CMPGT,
                                                                      OperatorType::CMPLT,
                                                                      OperatorType::CMPLE,
                                                                      OperatorType::CMPGE,
                                                                      OperatorType::SEXT,
                                                                      OperatorType::ZEXT,
                                                                      OperatorType::FPEXT,
                                                                      OperatorType::TRUNC,
                                                                      OperatorType::ITOF,
                                                                      OperatorType::FTOI,
    };

    /*! "Extended operators": makes no sense when backtracking, list them here */
    const std::set<OperatorType> ExtendedOpList = {OperatorType::SEXT,
                                                   OperatorType::ZEXT,
                                                   OperatorType::FPEXT,
                                                   OperatorType::TRUNC,
                                                   OperatorType::ITOF,
                                                   OperatorType::FTOI};
    /*! Maps operator types to string for printing out */
    const std::map<OperatorType,std::string> operatorNames = {{OperatorType::ADD,
                                                               "ADD"},
                                                              {OperatorType::SUB,
                                                               "SUB"},
                                                              {OperatorType::MUL,
                                                               "MUL"},
                                                              {OperatorType::DIV,
                                                               "DIV"},
                                                              {OperatorType::REM,
                                                               "REM"},
                                                              {OperatorType::LOAD,
                                                               "LOAD"},
                                                              {OperatorType::STORE,
                                                               "STORE"},
                                                              {OperatorType::ARRAY,
                                                               "ARRAY"},
                                                              {OperatorType::UBRANCH,
                                                               "UBR"},
                                                              {OperatorType::CBRANCH,
                                                               "CBR"},
                                                              {OperatorType::AND,
                                                               "AND"},
                                                              {OperatorType::OR,
                                                               "OR"},
                                                              {OperatorType::XOR,
                                                               "XOR"},
                                                              {OperatorType::SHL,
                                                               "SHL"},
                                                              {OperatorType::SHR,
                                                               "SHR"},
                                                              {OperatorType::FCALL,
                                                               "FCALL"},
                                                              {OperatorType::FRET,
                                                               "FRET"},
                                                              {OperatorType::CMPEQ,
                                                               "CMPEQ"},
                                                              {OperatorType::CMPNE,
                                                               "CMPNE"},
                                                              {OperatorType::CMPGT,
                                                               "CMPGT"},
                                                              {OperatorType::CMPLT,
                                                               "CMPLT"},
                                                              {OperatorType::CMPLE,
                                                               "CMPLE"},
                                                              {OperatorType::CMPGE,
                                                               "CMPGE"},
                                                              {OperatorType::ZEXT,
                                                               "ZEXT"},
                                                              {OperatorType::SEXT,
                                                               "SEXT"},
                                                              {OperatorType::FPEXT,
                                                               "FPEXT"},
                                                              {OperatorType::TRUNC,
                                                               "TRUNC"},
                                                              {OperatorType::ITOF,
                                                               "ITOF"},
                                                              {OperatorType::FTOI,
                                                               "FTOI"},
    };
    /*! Grouping of INT types for convenience */
    const std::set<OperandType> OpSupportIntOnly = {OperandType::INT8,
                                                    OperandType::INT16,
                                                    OperandType::INT32,
                                                    OperandType::INT64,
                                                    OperandType::POINTER};
    /*! Grouping of FLOAT types for convenience */
    const std::set<OperandType> OpSupportFpOnly = {OperandType::FLOAT32,
                                                   OperandType::FLOAT64,
                                                   OperandType::POINTER};
    /*! Grouping of INT & FLOAT types for convenience */
    const std::set<OperandType> OpSupportIntAndFp = {OperandType::INT8,
                                                     OperandType::INT16,
                                                     OperandType::INT32,
                                                     OperandType::INT64,
                                                     OperandType::FLOAT32,
                                                     OperandType::FLOAT64,
                                                     OperandType::POINTER};

    /*! Maps operator to supported types to optimize table size */
    const std::map<OperatorType, const std::set<OperandType>> operatorTypeSupport = {
        {OperatorType::ADD, OpSupportIntAndFp},
        {OperatorType::SUB, OpSupportIntAndFp},
        {OperatorType::MUL, OpSupportIntAndFp},
        {OperatorType::DIV, OpSupportIntAndFp},
        {OperatorType::REM, OpSupportIntAndFp},
        {OperatorType::AND, OpSupportIntOnly},
        {OperatorType::OR, OpSupportIntOnly},
        {OperatorType::XOR, OpSupportIntOnly},
        {OperatorType::SHR, OpSupportIntOnly},
        {OperatorType::SHL, OpSupportIntOnly},
        {OperatorType::LOAD, OpSupportIntAndFp},
        {OperatorType::STORE, OpSupportIntAndFp},
        {OperatorType::CMPEQ, OpSupportIntAndFp},
        {OperatorType::CMPNE, OpSupportIntAndFp},
        {OperatorType::CMPGT, OpSupportIntAndFp},
        {OperatorType::CMPLT, OpSupportIntAndFp},
        {OperatorType::CMPGE, OpSupportIntAndFp},
        {OperatorType::CMPLE, OpSupportIntAndFp},
        {OperatorType::ARRAY, {OperandType::VOID}},
        {OperatorType::FCALL, {OperandType::VOID}},
        {OperatorType::FRET, {OperandType::VOID}},
        {OperatorType::UBRANCH, {OperandType::VOID}},
        {OperatorType::CBRANCH, {OperandType::VOID}},
        {OperatorType::SEXT, OpSupportIntOnly},
        {OperatorType::ZEXT, OpSupportIntOnly},
        {OperatorType::FPEXT, OpSupportFpOnly},
        {OperatorType::TRUNC, OpSupportIntAndFp},
        {OperatorType::ITOF, OpSupportIntOnly},
        {OperatorType::FTOI, OpSupportFpOnly},
    };
    /*! List of supported types for convenience */
    const std::array<OperandType,operandTypeCount> operandTypes = {OperandType::INT8,
                                                                   OperandType::INT16,
                                                                   OperandType::INT32,
                                                                   OperandType::INT64,
                                                                   OperandType::FLOAT32,
                                                                   OperandType::FLOAT64,
                                                                   OperandType::POINTER,
                                                                   OperandType::VOID};
    /*! Maps type to string for printing out */
    const std::map<OperandType,std::string> operandNames = {{OperandType::INT8,
                                                             "INT8"},
                                                            {OperandType::INT16,
                                                             "INT16"},
                                                            {OperandType::INT32,
                                                             "INT32"},
                                                            {OperandType::INT64,
                                                             "INT64"},
                                                            {OperandType::FLOAT32,
                                                             "FLOAT"},
                                                            {OperandType::FLOAT64,
                                                             "DOUBLE"},
                                                            {OperandType::POINTER,
                                                             "POINTER"},
                                                            {OperandType::VOID,
                                                             "VOID"}};
    /*! A line of an OpTable */
    typedef std::map<OperandType, unsigned int> OpTableLine;
    /*! Entire contents of an OpTable */
    typedef std::map<OperatorType, OpTableLine*> OpTableContents;

    /*!
      OpTable class
      Stores operator counts by supported type and provides manipulation functions
    */
    class OpTable
    {
    private:
        OpTableContents theTable;
        bool extendedOpValid;
    public:
        OpTable(bool extendedOperators=false);

        /*! Get if extended operators are counted
         */
        bool GetExtendedValid(void) { return extendedOpValid; }

        /*! Modify extended operator validity
         */
        void SetExtendedValid(bool valid) { extendedOpValid = valid; }

        /*! Increment an entry on the table
          \param opndType the operand type
          \param opType the operator type
        */
        void IncrementValue(OperandType opndType, OperatorType opType);

        /*! Set value of an entry on the table
          \param opndType the operand type
          \param opType the operator type
          \param value the value to be set
        */
        void SetValue(OperandType opndType, OperatorType opType, unsigned int value);

        /*! Get value of an entry of the table
          \param opndType the operand type
          \param opType the operator type
          \return the current value
        */
        unsigned int GetCount(OperandType opndType, OperatorType opType);

        /*! Check if table is empty, i.e. all entries' value is zero
          \return true if empty
        */
        bool isEmpty(void);

        /*! Multiply element-wise by scalar
          \param c a scalar value
        */
        void constantMultiply(unsigned int c);

        /*! Serialize table to integer array
          \return integer array of table size containing table values
        */
        unsigned int * dumpTable(void);

        /*! Populate table from serialized data
          \param data pointer to an integer array containing table values
        */
        void populate(unsigned int * data);

        /*! Print formatted table
          \param output an ostream object
          \param separator separator character*/
        void print(std::ostream &output, char separator=' ');

        /*! Print JSON-formatted table
          \param output an ostream object
        */
        void printJSON(std::ostream & output);

        /*! Get JSON data
         */
        miniJSON::JSONObject * getJSON(void);

        /*! Calculate and return table size
          \return table size (number of elements)
        */
        static unsigned int getTableSize(void);

        /*! Overload multiplication by scalar */
        friend OpTable operator*(const OpTable& table, unsigned int c);
        /*! Overload multiplication by scalar */
        friend OpTable operator*(unsigned int c, const OpTable& table);
        /*! Overload table x table multiplication */
        friend OpTable operator*(const OpTable& table1, const OpTable& table2);
        /*! Overload table adding */
        friend OpTable operator+(const OpTable& table1, const OpTable& table2);
    };

    /* Helper functions */

    /*! Converts a LLVM Instruction opcode to OperatorType
      \param opcode the instruction opcode
      \param someCondition used for branches, determine if conditional or not
      \return a member of OperatorType
    */
    OperatorType LLVMOpcodeToOperatorType(unsigned opcode, bool someCondition=false);

    /*! Converts a LLVM ICMP/FCMP instruction predicate to OperatorType
      \param pred the predicate
      \return a member of OperatorType
    */
    OperatorType LLVMCmpPredicateToOperatorType(llvm::CmpInst::Predicate pred);

    /*! Converts a LLVM type to OperandType
      \param llvmType the type
      \return a member of OperandType
    */
    OperandType LLVMTypeToOperandType(llvm::Type * llvmType);
}

#endif /* OPTABLE_H */
