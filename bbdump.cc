/*! \file bbdump.cc
    \brief Result output library for profiling
    \author Bruno Morais <brunosmmm@gmail.com>
    \since 10/2015
*/
#include <iostream>
#include <fstream>
#include <vector>
#include "ircount.hh"
#include "minijson.hh"

//#define VERBOSE

namespace IRProfiler
{
    void DumpCounters(unsigned int * array, unsigned int arraySize,
                      unsigned int * globalOpTable, unsigned int globalOpTableSize,
                      bool extendedOpValid, char * saveCounts)
    {

        if ((globalOpTableSize / OpTable::getTableSize()) != arraySize)
        {
            std::cout << "WARNING: basic block count and OpTable sizes do not match" << std::endl;
        }

        //convert back to OpTable
        if (globalOpTableSize % OpTable::getTableSize())
        {
            std::cout << "WARNING: incorrectly sized global OpTable" << std::endl;
            globalOpTableSize = globalOpTableSize - (globalOpTableSize % OpTable::getTableSize());
        }

        std::vector<OpTable> OpTablePerBB(globalOpTableSize / OpTable::getTableSize());
        unsigned int i = 0;

        std::cout << "INFO: execution finished (" << arraySize
                  << " basic blocks total)" << std::endl;


        miniJSON::JSONObject * bbCounts = new miniJSON::JSONObject;
        while (globalOpTableSize)
        {
            miniJSON::JSONField * basicBlockCount = new miniJSON::JSONField(std::to_string(i), array[i]);
            bbCounts->AddField(basicBlockCount);

            OpTablePerBB[i].populate(globalOpTable + i*OpTable::getTableSize());

            //multiply by basic block count
            if (i < arraySize)
            {
                OpTablePerBB[i].constantMultiply(array[i]);
            }

            i++;
            globalOpTableSize -= OpTable::getTableSize();
        }

        //output JSON for counts
        std::ofstream jsonCounts;
        if (saveCounts)
        {
            miniJSON::JSONWriter bbCountsOutput;
            bbCountsOutput.SetRoot(bbCounts);

            std::string filePrefix(saveCounts);
            std::cout << "INFO: saving basic block counts to " << saveCounts << ".bb.json" << std::endl;
            jsonCounts.open(filePrefix+".bb.json");
            bbCountsOutput.Dump(jsonCounts);
            jsonCounts.close();
        }

        OpTable totalCount(extendedOpValid);

        for (OpTable table : OpTablePerBB)
        {
            totalCount = totalCount + table;
        }

        //dump final table
        if (saveCounts)
        {
            std::string filePrefix(saveCounts);
            std::ofstream jsonOpCounts;
            jsonOpCounts.open(filePrefix+".prof.json");
            totalCount.printJSON(jsonOpCounts);
            jsonOpCounts.close();
            std::cout << "INFO: Total counts saved to " << saveCounts << ".prof.json" << std::endl;

        }
        else
        {
            //dump to stderr
            std::cout << "INFO: Total counts dumped to stderr" << std::endl;
            totalCount.printJSON(std::cerr);
        }
    }
}

//must make this ugly hack for LLVM to see the function
extern "C" void DumpCounters(unsigned int* array, unsigned int arraySize,
                             unsigned int* globalOpTable, unsigned int globalOpTableSize,
                             char extendedOpValid, char * saveCounts)
{
    IRProfiler::DumpCounters(array, arraySize, globalOpTable,
                             globalOpTableSize, bool(extendedOpValid),
                             saveCounts);
}
