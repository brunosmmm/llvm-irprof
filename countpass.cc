/*! \file countpass.cc
    \brief LLVM optimization pass for IR profiling and instrumentation
    \author Bruno Morais <brunosmmm@gmail.com>
    \since 10/2015
*/
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/ValueSymbolTable.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/CommandLine.h"
#include "optable.hh"
#include "ircount.hh"
#include "minijson.hh"
#include <iostream>
#include <fstream>
#include <list>
#include <string>
namespace
{

    //create command line options
    static llvm::cl::opt<bool> NoBacktrack("no-backtrack", llvm::cl::desc("Do not backtrack binary operation operand types"));
    static llvm::cl::opt<std::string> SaveCounts("save-counts", llvm::cl::desc("Save basic block execution / operation count as JSON"), llvm::cl::value_desc("fileprefix"));

    //
    class OpCountPass : public llvm::ModulePass
    {
    public:
        static char ID;

        OpCountPass() : llvm::ModulePass(ID) {};

        //function pass
        bool runOnModule(llvm::Module &llvmModule) override;
        //bool doFinalization(llvm::Module &llvmModule) override;

        //require other passes
        void getAnalysisUsage(llvm::AnalysisUsage &Info) const override;
    private:
        void InsertCounterIncrement(llvm::BasicBlock &bb, unsigned int globalArrayIndex, llvm::GlobalVariable * globalArray);
        void InsertCounterDump(llvm::Function *main, llvm::GlobalVariable * globalArray, unsigned int globalArraySize, llvm::Function* dump,
                               llvm::GlobalVariable * globalOpTable, unsigned int globalOpTableSize, llvm::GlobalVariable * JSONCounts);
        void ProcessBasicBlock(llvm::BasicBlock *basicBlock, IRProfiler::IRFunctionStats * funcStats, unsigned int currentBBNum, miniJSON::JSONObject * basicBlockCounts);
        void ProcessInstruction(llvm::Instruction *instruction, IRProfiler::OpTable* table);
        void ProcessFunction(llvm::Function &llvmFunction, unsigned int currentBBNum, miniJSON::JSONObject * basicBlockCounts);
        llvm::Type* BackTrackOperand(llvm::Value *operand, unsigned int rLevel=0);
        llvm::Type* DecideType(llvm::Type* opnd1, llvm::Type* opnd2);

        //overall operator counts
        IRProfiler::IRProgramStats opStats;
    };

    void copyArrayIntoArrayOffset(llvm::Constant** dest, unsigned int destOffset, unsigned int * source, unsigned int sourceSize, llvm::Module &M)
    {
        unsigned int i = 0;
        while (sourceSize--)
        {
            dest[destOffset+i] = llvm::ConstantInt::get(llvm::IntegerType::get(M.getContext(), 32), int(source[i++]));
        }
    }

    void saveFunctionName(llvm::Constant** dest, unsigned int offset, const char * name, llvm::Module &M)
    {
        unsigned int i = 0;
        while (*name)
        {
            dest[offset+i] = llvm::ConstantInt::get(llvm::IntegerType::get(M.getContext(), 8), *name);
            i++;
            name++;
        }

        dest[offset+i] = llvm::ConstantInt::get(llvm::IntegerType::get(M.getContext(), 8), ';');
    }

    bool OpCountPass::runOnModule(llvm::Module &llvmModule)
    {
        //acquire main function
        llvm::Function * appMain;
        miniJSON::JSONObject * jsonRoot = new miniJSON::JSONObject();
        appMain = llvmModule.getFunction("main");

        if (!appMain)
        {
            std::cerr << "ERROR: could not locate main function" << std::endl;
            return false;
        }

        std::cerr << "Entering module: " << llvmModule.getName().str() << std::endl;

        miniJSON::JSONObject * jsonFunctions = new miniJSON::JSONObject();
        miniJSON::JSONField * jsonFunctionsContainer = new miniJSON::JSONField("Functions",
                                                                               jsonFunctions);

        miniJSON::JSONObject * jsonBasicBlocks = new miniJSON::JSONObject;
        miniJSON::JSONField * jsonBasicBlocksContainer = new miniJSON::JSONField("BasicBlocks",
                                                                                 jsonBasicBlocks);

        jsonRoot->AddField(jsonFunctionsContainer);
        jsonRoot->AddField(jsonBasicBlocksContainer);

        //iterate through functions, count basic block number
        unsigned int bbCount = 0;
        for (auto &function : llvmModule)
        {
            miniJSON::JSONObject * jsonFunctionBounds;
            miniJSON::JSONField * jsonFunctionStart;
            if  (function.size() > 0)
            {
                jsonFunctionBounds = new miniJSON::JSONObject;
                jsonFunctionStart = new miniJSON::JSONField("startBB", bbCount);
                jsonFunctionBounds->AddField(jsonFunctionStart);
            }

            ProcessFunction(function, bbCount, jsonBasicBlocks);
            bbCount += function.size();

            if (function.size() > 0)
            {
                miniJSON::JSONField * jsonFunctionEnd = new miniJSON::JSONField("endBB", bbCount-1);
                jsonFunctionBounds->AddField(jsonFunctionEnd);

                miniJSON::JSONField * jsonFunctionDescr = new miniJSON::JSONField(function.getName().str(), jsonFunctionBounds);
                jsonFunctions->AddField(jsonFunctionDescr);
            }
        }

        miniJSON::JSONWriter jsonOut;
        jsonOut.SetRoot(jsonRoot);

        //save detailed data to file
        if (SaveCounts.size() > 0)
        {
            std::ofstream detailOut;
            detailOut.open(SaveCounts+".stat.json");
            jsonOut.Dump(detailOut);
            detailOut.close();
        }

        std::cerr << "INFO: module has " << bbCount << " basic blocks." << std::endl;
        //initial counts acquired, instrument code now

        //make proper array type for storing bb counts
        llvm::Type* bbInstrumentType = llvm::ArrayType::get(llvm::Type::getInt32Ty(llvmModule.getContext()), bbCount);

        //make global variable to store counts
        llvm::GlobalVariable * bbCounters;
        bbCounters = new llvm::GlobalVariable(llvmModule,
                                              bbInstrumentType,
                                              false,
                                              llvm::GlobalValue::InternalLinkage,
                                              llvm::Constant::getNullValue(bbInstrumentType),
                                              "bbCounts");

        //instrument blocks
        std::cerr << "INFO: instrumenting code now" << std::endl;

        bbCount = 0;
        for (auto &function : llvmModule)
        {
            for (auto &block : function)
            {
                InsertCounterIncrement(block, bbCount++, bbCounters);
            }
        }

        llvm::Constant** opTableArrayContents;
        llvm::Constant** funcNamesArrayContents;
        unsigned int opTableArrayContentsSize = bbCount*IRProfiler::OpTable::getTableSize();
        //allocate massive array
        opTableArrayContents = new llvm::Constant*[opTableArrayContentsSize];

        //copy each basic block's optable
        ///TODO: optables are horrible to access, rewrite the whole thing
        unsigned int tabOffset = 0;
        unsigned int funcNameLength = 0;
        for (auto &function : llvmModule)
        {
            IRProfiler::IRFunctionStats* funcStats;
            funcStats = opStats.getFunctionStats(function.getName().str());
            funcNameLength += function.getName().str().size() + 1;

            if (!funcStats)
            {
                std::cerr << "ERROR: unknown error" << std::endl;
                continue;
            }

            for (auto &block : function)
            {
                IRProfiler::IRBasicBlockStats* bbStats;
                IRProfiler::OpTable* bbTable;
                unsigned int* serializedTable;
                bbStats = funcStats->getBasicBlockStats(&block);

                if (!bbStats)
                {
                    std::cerr << "ERROR: unknown error" << std::endl;
                    continue;
                }

                bbTable = bbStats->accessTable();

                if (!bbTable)
                {
                    std::cerr << "ERROR: unknown error" << std::endl;
                    continue;
                }

                serializedTable = bbTable->dumpTable();

                //copy into massive array
                copyArrayIntoArrayOffset(opTableArrayContents,
                                         tabOffset,
                                         serializedTable,
                                         IRProfiler::OpTable::getTableSize(),
                                         llvmModule);
                delete serializedTable;

                tabOffset += IRProfiler::OpTable::getTableSize();

                if (tabOffset > opTableArrayContentsSize)
                {
                    std::cerr << "ERROR: unknown error" << std::endl;
                    break;
                }
            }
        }

        //save function names
        funcNamesArrayContents = new llvm::Constant*[funcNameLength];
        unsigned int nameOffset = 0;
        for (auto &function : llvmModule)
        {
            saveFunctionName(funcNamesArrayContents, nameOffset, function.getName().str().c_str(), llvmModule);
            nameOffset += function.getName().str().size() + 1;
        }

        //insert dummy function
        llvm::Constant* dumpFunction;
        dumpFunction = llvmModule.getOrInsertFunction("DumpCounters",
                                                      llvm::Type::getVoidTy(llvmModule.getContext()),
                                                      llvm::Type::getInt32PtrTy(llvmModule.getContext()),
                                                      llvm::Type::getInt32Ty(llvmModule.getContext()),
                                                      llvm::Type::getInt32PtrTy(llvmModule.getContext()),
                                                      llvm::Type::getInt32Ty(llvmModule.getContext()),
                                                      llvm::Type::getInt8Ty(llvmModule.getContext()),
                                                      llvm::Type::getInt8PtrTy(llvmModule.getContext()),
                                                      NULL);

        llvm::Function* dumpCounters;
        dumpCounters = dynamic_cast<llvm::Function*>(dumpFunction);

        //maximum ugliness: embed IRProgramStats instance into the code?
        llvm::ArrayType* opTableArrayType = llvm::ArrayType::get(llvm::Type::getInt32Ty(llvmModule.getContext()), bbCount*IRProfiler::OpTable::getTableSize());

        //make constant out of the serialized tables
        llvm::Constant* opTableArray = llvm::ConstantArray::get(opTableArrayType,
                                                                llvm::ArrayRef<llvm::Constant*>(opTableArrayContents, opTableArrayContentsSize));

        llvm::GlobalVariable * opTableVar = new llvm::GlobalVariable(llvmModule,
                                                                     opTableArrayType,
                                                                     true,
                                                                     llvm::GlobalValue::InternalLinkage,
                                                                     opTableArray,
                                                                     "opTab");

        //embbed more stuff for more verbosity
        llvm::ArrayType* funcNameArrayType = llvm::ArrayType::get(llvm::Type::getInt8Ty(llvmModule.getContext()), funcNameLength);

        //function names!
        llvm::Constant* funcNameArray = llvm::ConstantArray::get(funcNameArrayType,
                                                                 llvm::ArrayRef<llvm::Constant*>(funcNamesArrayContents, funcNameLength));

        llvm::GlobalVariable* funcNameVar = new llvm::GlobalVariable(llvmModule,
                                                                     funcNameArrayType,
                                                                     true,
                                                                     llvm::GlobalValue::InternalLinkage,
                                                                     funcNameArray,
                                                                     "fnames");

        //embbed SaveCounts option information
        //just for scope reasons
        llvm::GlobalVariable * JSONCounts = NULL;
        if (SaveCounts.size() > 0)
        {
            llvm::Constant** JSONCountsContents = new llvm::Constant*[SaveCounts.size()+1];
            const char * JSONFname = SaveCounts.c_str();
            unsigned int off = 0;
            while (*JSONFname)
            {
                JSONCountsContents[off] = llvm::ConstantInt::get(llvm::IntegerType::get(llvmModule.getContext(), 8), *JSONFname);
                JSONFname++;
                off++;
            }
            JSONCountsContents[off] = llvm::ConstantInt::get(llvm::IntegerType::get(llvmModule.getContext(), 8), 0);
            llvm::ArrayType* JSONCountsType = llvm::ArrayType::get(llvm::Type::getInt8Ty(llvmModule.getContext()), SaveCounts.size()+1);
            llvm::Constant* JSONCountsArray = llvm::ConstantArray::get(JSONCountsType, llvm::ArrayRef<llvm::Constant*>(JSONCountsContents, SaveCounts.size()+1));
            llvm::GlobalVariable* JSONCountsVar = new llvm::GlobalVariable(llvmModule,
                                                                            JSONCountsType,
                                                                            true,
                                                                            llvm::GlobalValue::InternalLinkage,
                                                                            JSONCountsArray,
                                                                            "countfile");

            JSONCounts = JSONCountsVar;
        }


        //insert function to dump counts at execution end
        InsertCounterDump(appMain, bbCounters, bbCount, dumpCounters, opTableVar, opTableArrayContentsSize, JSONCounts);

        std::cerr << "INFO: analysis and instrumentation completed." << std::endl;

        return true;
    }

    void OpCountPass::InsertCounterIncrement(llvm::BasicBlock &bb, unsigned int globalArrayIndex, llvm::GlobalVariable * globalArray)
    {

        //insert instrumentation code
        llvm::Instruction* firstInstruction;
        //generate instructions
        llvm::GetElementPtrInst* getArrayElement;
        llvm::LoadInst* loadCounter;
        llvm::BinaryOperator* incrementCounter;
        llvm::StoreInst* storeCounter;

        //get first instruction
        firstInstruction = bb.begin();

        //detect if this is a phi node
        if (llvm::PHINode::classof(firstInstruction))
        {
            //phi nodes are not desirable because they hide the underlying conditional structure
            std::cerr << "WARNING: PHI node detected" << std::endl;

            //instrumentation will be inserted AFTER phi node or else this is a LLVM IR error!
            firstInstruction = bb.getFirstNonPHI();
        }

        //create getelementptr
        llvm::Value* indexList[2] = {llvm::ConstantInt::get(llvm::IntegerType::get(bb.getContext(), 32), 0),
                                     llvm::ConstantInt::get(llvm::IntegerType::get(bb.getContext(), 32), globalArrayIndex)};
        getArrayElement = llvm::GetElementPtrInst::Create(globalArray->getType()->getPointerElementType(),
                                                          globalArray,
                                                          llvm::ArrayRef<llvm::Value*>(indexList, 2),
                                                          "i",
                                                          firstInstruction);
        //create load
        loadCounter = new llvm::LoadInst(getArrayElement,
                                         "i",
                                         firstInstruction);

        //create increment
        incrementCounter = llvm::BinaryOperator::Create(llvm::Instruction::BinaryOps::Add,
                                                        loadCounter,
                                                        llvm::ConstantInt::get(llvm::IntegerType::get(bb.getContext(), 32), 1),
                                                        "i",
                                                        firstInstruction);

        //create store
        storeCounter = new llvm::StoreInst(incrementCounter,
                                           getArrayElement,
                                           firstInstruction);

        //done!
    }

    void OpCountPass::InsertCounterDump(llvm::Function *main, llvm::GlobalVariable * globalArray, unsigned int globalArraySize, llvm::Function* dump,
                                        llvm::GlobalVariable * globalOpTable, unsigned int globalOpTableSize, llvm::GlobalVariable * JSONCounts)
    {
        llvm::GetElementPtrInst* getArrayElement, * getGlobalOpTable, * getJSONCountsFilename = NULL;
        llvm::CallInst* callDump;

        for (auto &block : *main)
        {
            llvm::TerminatorInst* blockTerminator = block.getTerminator();

            if (llvm::ReturnInst::classof(blockTerminator))
            {
                //program could exit here, insert call

                //create getelementptr
                llvm::Value* indexList[2] = {llvm::ConstantInt::get(llvm::IntegerType::get(main->getContext(), 32), 0),
                                             llvm::ConstantInt::get(llvm::IntegerType::get(main->getContext(), 32), 0)};
                getArrayElement = llvm::GetElementPtrInst::Create(globalArray->getType()->getPointerElementType(),
                                                                  globalArray,
                                                                  llvm::ArrayRef<llvm::Value*>(indexList, 2),
                                                                  "i",
                                                                  blockTerminator);
                //get serialized global OpTable
                getGlobalOpTable = llvm::GetElementPtrInst::Create(globalOpTable->getType()->getPointerElementType(),
                                                                   globalOpTable,
                                                                   llvm::ArrayRef<llvm::Value*>(indexList, 2),
                                                                   "i",
                                                                   blockTerminator);

                //create call
                llvm::Value* dumpArgs[6] = {getArrayElement,
                                            llvm::ConstantInt::get(llvm::IntegerType::get(main->getContext(), 32), globalArraySize),
                                            getGlobalOpTable,
                                            llvm::ConstantInt::get(llvm::IntegerType::get(main->getContext(), 32), globalOpTableSize),
                                            llvm::ConstantInt::get(llvm::IntegerType::get(main->getContext(), 8), int(NoBacktrack)),
                                            NULL};

                //get count output filename
                if (JSONCounts)
                {
                    getJSONCountsFilename = llvm::GetElementPtrInst::Create(JSONCounts->getType()->getPointerElementType(),
                                                                            JSONCounts,
                                                                            llvm::ArrayRef<llvm::Value*>(indexList, 2),
                                                                            "i",
                                                                            blockTerminator);
                    dumpArgs[5] = getJSONCountsFilename;
                }
                else
                {
                    dumpArgs[5] = llvm::ConstantPointerNull::get(llvm::Type::getInt8PtrTy(main->getContext()));
                }



                callDump = llvm::CallInst::Create(dump,
                                                  llvm::ArrayRef<llvm::Value*>(dumpArgs, 6),
                                                  "",
                                                  blockTerminator);
            }
        }
    }

    void OpCountPass::ProcessFunction(llvm::Function &llvmFunction, unsigned int currentBBNum,
                                      miniJSON::JSONObject * basicBlockCounts)
    {
        unsigned int bbCount = 0;
        IRProfiler::IRFunctionStats fStats(llvmFunction.getName().str());

        std::cerr << "Entering function: " << llvmFunction.getName().str() << std::endl;

        //iterate through basic blocks
        for (llvm::Function::iterator basicBlock : llvmFunction)
        {
            //process each basic block, hand down function stats pointer for counting
            ProcessBasicBlock(basicBlock, &fStats, currentBBNum+bbCount, basicBlockCounts);
            bbCount++;
        }

        //insert function stats entry
        fStats.setBBCount(bbCount);
        opStats.addFunctionStats(fStats);

        std::cerr << "Function has " << bbCount << " basic blocks" << std::endl;

        std::cerr << "Symbol table: " << std::endl;
        llvm::ValueSymbolTable * symTab = &(llvmFunction.getValueSymbolTable());

        for (llvm::ValueSymbolTable::ValueMap::iterator value = symTab->begin();
             value != symTab->end(); value++)
        {
            std::cerr << value->getKey().str() << " -> " <<
                symTab->lookup(value->getKey().str()) << std::endl;
        }

        //IRProfiler::OpTable fTable = fStats.getOverallOpTable();
    }

    void OpCountPass::ProcessBasicBlock(llvm::BasicBlock *basicBlock, IRProfiler::IRFunctionStats* funcStats, unsigned int currentBBNum, miniJSON::JSONObject * basicBlockCounts)
    {
        //allocate structures
        IRProfiler::OpTable* table;
        table = new IRProfiler::OpTable(NoBacktrack);
        IRProfiler::IRBasicBlockStats bb(table, basicBlock);

        //iterate through instructions
        std::cerr << "Entering basic block: #" << currentBBNum << " (" << basicBlock << ")" << std::endl;
        for (llvm::BasicBlock::iterator instruction : *basicBlock)
        {
            //process and count instructions
            ProcessInstruction(instruction, table);
        }

        //add basic block stats to function
        funcStats->addBasicBlockStats(bb);

        //basic block stats
        if (!table->isEmpty())
        {
            // std::cerr << "Basic block totals: " << std::endl;
            //table->print();
            miniJSON::JSONField * bbTable = new miniJSON::JSONField(std::to_string(currentBBNum),
                                                                    table->getJSON());
            basicBlockCounts->AddField(bbTable);
        }
        else
        {
            std::cerr << "INFO: No operations in basic block" << std::endl;
        }
    }

    void OpCountPass::ProcessInstruction(llvm::Instruction *instruction, IRProfiler::OpTable* table)
    {
        switch(instruction->getOpcode())
        {
        case llvm::Instruction::FAdd:
        case llvm::Instruction::Add:
        case llvm::Instruction::Sub:
        case llvm::Instruction::FSub:
        case llvm::Instruction::Mul:
        case llvm::Instruction::FMul:
        case llvm::Instruction::UDiv:
        case llvm::Instruction::SDiv:
        case llvm::Instruction::FDiv:
        case llvm::Instruction::And:
        case llvm::Instruction::Or:
        case llvm::Instruction::Xor:
        case llvm::Instruction::Shl:
        case llvm::Instruction::LShr:
        case llvm::Instruction::AShr:
        case llvm::Instruction::URem:
        case llvm::Instruction::SRem:
        case llvm::Instruction::FRem:
        {
            llvm::Type * result;

            if (!NoBacktrack)
            {

                std::cerr << "* Inspecting binary operation: ";
                instruction->dump();

                llvm::Type * opnd1, * opnd2;
                //backtrack first operand
                opnd1 = BackTrackOperand(instruction->getOperand(0));
                //backtrack second operand
                opnd2 = BackTrackOperand(instruction->getOperand(1));
                //decide operation type and put into table
                result = DecideType(opnd1, opnd2);
            }
            else
            {
                result = instruction->getOperand(0)->getType();
            }

            if (result)
            {

                if ((IRProfiler::LLVMTypeToOperandType(result) == IRProfiler::OperandType::UNKNOWN)
                    || (IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()) == IRProfiler::OperatorType::UNKNOWN))
                {
                    //unknown type or operand, abort
                    break;
                }
                if (!NoBacktrack)
                {
                    std::cerr << "INFO: counted a "
                              << IRProfiler::operandNames.at(IRProfiler::LLVMTypeToOperandType(result))
                              << " "
                              << IRProfiler::operatorNames.at(IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()))
                              << std::endl;
                }

                table->IncrementValue(IRProfiler::LLVMTypeToOperandType(result),
                                      IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()));
            }

            break;
        }

        case llvm::Instruction::FCmp:
        case llvm::Instruction::ICmp:
        {
            llvm::Type * result;
            llvm::CmpInst * compare;
            llvm::CmpInst::Predicate cmpKind;
            compare = dynamic_cast<llvm::CmpInst*>(instruction);

            if (!compare)
            {
                std::cerr << "ERROR: error analyzing compare instruction:";
                instruction->dump();
                break;
            }

            //get predicate
            cmpKind = compare->getPredicate();

            if (!NoBacktrack)
            {
                std::cerr << "* Inspecting compare operation: ";
                instruction->dump();

                llvm::Type * opnd1, * opnd2;
                opnd1 = BackTrackOperand(instruction->getOperand(0));
                opnd2 = BackTrackOperand(instruction->getOperand(1));
                result = DecideType(opnd1, opnd2);

            }
            else
            {
                result = instruction->getOperand(0)->getType();
            }

            if (result)
            {
                if ((IRProfiler::LLVMTypeToOperandType(result) == IRProfiler::OperandType::UNKNOWN)
                    || (IRProfiler::LLVMCmpPredicateToOperatorType(cmpKind) == IRProfiler::OperatorType::UNKNOWN))
                {
                    //unknown type or operand, abort
                    break;
                }

                table->IncrementValue(IRProfiler::LLVMTypeToOperandType(result),
                                      IRProfiler::LLVMCmpPredicateToOperatorType(cmpKind));

            }

            break;
        }
        case llvm::Instruction::Load:
        {
            llvm::Type* loadedType;
            loadedType = instruction->getOperand(0)->getType()->getPointerElementType();

            if ((IRProfiler::LLVMTypeToOperandType(loadedType) == IRProfiler::OperandType::UNKNOWN)
                || (IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()) == IRProfiler::OperatorType::UNKNOWN))
            {
                //unknown type or operand, abort
                break;
            }

            table->IncrementValue(IRProfiler::LLVMTypeToOperandType(loadedType),
                                  IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()));

            break;
        }
        case llvm::Instruction::Store:
        {
            llvm::Type* loadedType;
            loadedType = instruction->getOperand(0)->getType();

            if ((IRProfiler::LLVMTypeToOperandType(loadedType) == IRProfiler::OperandType::UNKNOWN)
                || (IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()) == IRProfiler::OperatorType::UNKNOWN))
            {
                //unknown type or operand, abort
                break;
            }

            table->IncrementValue(IRProfiler::LLVMTypeToOperandType(loadedType),
                                  IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()));

            break;
        }
        case llvm::Instruction::GetElementPtr:
        {
            llvm::Type* arrayType;
            arrayType = instruction->getOperand(0)->getType()->getPointerElementType();

            //problematic because it can be struct or array, which can in turn be nested
            if (arrayType->isArrayTy())
            {
                //don't care about array type! array access is all we need
                table->IncrementValue(IRProfiler::OperandType::VOID,
                                      IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()));
            }
            else if (arrayType->isStructTy())
            {
                //to detect type one must look at the struct declaration. treat as array access
                //for now
                table->IncrementValue(IRProfiler::OperandType::VOID,
                                      IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()));
            }
            break;
        }
        case llvm::Instruction::Br:
        {
            llvm::BranchInst* branch;
            branch = dynamic_cast<llvm::BranchInst*>(instruction);

            table->IncrementValue(IRProfiler::OperandType::VOID,
                                  IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode(), branch->isConditional()));

            break;
        }
        case llvm::Instruction::Ret:
        case llvm::Instruction::Call:
        {
            //POINTER type
            table->IncrementValue(IRProfiler::OperandType::VOID,
                                  IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()));

            break;
        }

        //instructions that are not relevant
        case llvm::Instruction::Alloca:
            break;

        //count sign extensions
        case llvm::Instruction::ZExt:
        case llvm::Instruction::SExt:
        case llvm::Instruction::FPExt:
        case llvm::Instruction::Trunc:
        case llvm::Instruction::FPTrunc:
        case llvm::Instruction::FPToUI:
        case llvm::Instruction::FPToSI:
        case llvm::Instruction::SIToFP:
        case llvm::Instruction::UIToFP:
            if (NoBacktrack)
            {
                table->IncrementValue(IRProfiler::LLVMTypeToOperandType(instruction->getOperand(0)->getType()),
                                      IRProfiler::LLVMOpcodeToOperatorType(instruction->getOpcode()));
            }
            break;

        default:

            std::cerr << "WARNING: ignored instruction: ";
            instruction->dump();
            break;
        }
    }

    llvm::Type* OpCountPass::DecideType(llvm::Type* opnd1, llvm::Type* opnd2)
    {
        //if both are null pointers
        if ((opnd1 == NULL) && (opnd2 == NULL))
        {
            return NULL;
        }

        if (opnd1)
        {
            if (!opnd2)
            {
                //return operand 1 type
                return opnd1;
            }

            if (opnd1 == opnd2)
            {
                //same type, doesn't matter
                return opnd1;
            }
            else
            {
                //operand types are different!

                //check for int/float for completeness?
                //cannot really happen because if a cast is detected, then
                //only the target type is returned.
                std::cerr << "WARNING: backtracked to two different types, selecting: ";

                //assume that a "bigger" type wins
                if ((opnd1->isIntegerTy() == opnd2->isIntegerTy()) ||
                    (opnd1->isFloatingPointTy() == opnd2->isFloatingPointTy()))
                {
                    if (opnd1->getPrimitiveSizeInBits() > opnd2->getPrimitiveSizeInBits())
                    {
                        opnd1->dump();
                        return opnd1;
                    }
                    else
                    {
                        opnd2->dump();
                        return opnd2;
                    }
                }
                else
                {
                    //cannot decide
                    std::cerr << "ERROR: could not decide operation type" << std::endl;
                    return NULL;
                }
            }

        }
        else
        {
            //opnd1 is constant
            if (opnd2)
            {
                return opnd2;
            }
        }

        return NULL;
    }

    llvm::Type* OpCountPass::BackTrackOperand(llvm::Value* operand, unsigned int rLevel)
    {
        llvm::Instruction * backtrack;
        llvm::Value * value;
        llvm::Type * type;

        //could be a constant, test
        if ((llvm::ConstantInt::classof(operand)) ||
            (llvm::ConstantFP::classof(operand)))
        {
            //no interest in constants, get out
            return NULL;
        }


        if (llvm::Instruction::classof(operand))
        {
            backtrack = dynamic_cast<llvm::Instruction*>(operand);
        }
        else
        {
            //TODO: check for other possible types, ptr<->int casts
            std::cerr << "WARNING: possible pointer to integer cast detected." << std::endl;

            if (llvm::IntegerType::classof(operand->getType()))
            {
                //for now treat as integer type (after cast)
                return operand->getType();
            }

            //unbiased, decide on other type
            return NULL;
        }

        //check if cast succeded
        if (!backtrack)
        {
            //TODO: check for ptrtoint operations!
            std::cerr << "ERROR: could not backtrack, invalid instruction: " << std::endl;
            return NULL;
        }

        if (backtrack->isBinaryOp())
        {
            llvm::Type * opnd1, * opnd2, * result;

            std::cerr << "INFO: recursing (" << rLevel << ") into: ";
            backtrack->dump();

            //backtrack more
            opnd1 = BackTrackOperand(backtrack->getOperand(0), rLevel+1);
            opnd2 = BackTrackOperand(backtrack->getOperand(1), rLevel+1);

            //compare operands and return correct
            result = DecideType(opnd1, opnd2);

            //dont print for each recursion level
            if (result)
            {
                std::cerr << "INFO: type decision (" << rLevel << ") is: ";
                result->dump();
            }

            return result;
        }
        else
        {
            //dead ends, get original types and count
            switch(backtrack->getOpcode())
            {
            case llvm::Instruction::FPExt:
            case llvm::Instruction::ZExt:
            case llvm::Instruction::SExt:
            {
                std::cerr << "** Backtracked to a type extension: ";
                backtrack->dump();

                //get original type
                value = backtrack->getOperand(0);
                type = value->getType();
                std::cerr << "*** Original type is: ";
                type->dump();

                return type;
            }
            case llvm::Instruction::FPTrunc:
            case llvm::Instruction::Trunc:
            {
                std::cerr <<  "** Backtracked to a truncation: ";
                backtrack->dump();

                value = backtrack->getOperand(0);
                type = value->getType();
                std::cerr << "*** Original type is: ";
                type->dump();

                return NULL; //don't interfere!
            }
            case llvm::Instruction::Load:
            {
                std::cerr << "** Backtracked to a load: ";
                backtrack->dump();

                //get type
                value = backtrack->getOperand(0);
                type = value->getType();
                std::cerr << "*** Original type is: ";
                type->getPointerElementType()->dump();

                return type->getPointerElementType();
            }
            case llvm::Instruction::FPToUI:
            case llvm::Instruction::FPToSI:
            {
                //cast float -> (unsigned) int
                std::cerr << "** Backtracked to a float->int cast: ";
                backtrack->dump();

                //get type
                value = backtrack->getOperand(0);
                type = value->getType();
                std::cerr << "*** Original type is: ";
                type->dump();

                return NULL; //eventually yields type casted to
            }
            case llvm::Instruction::UIToFP:
            case llvm::Instruction::SIToFP:
            {
                //cast (unsigned) int -> float
                std::cerr << "** Backtracked to a int->float cast: ";
                backtrack->dump();

                //get type
                value = backtrack->getOperand(0);
                type = value->getType();
                std::cerr << "*** Original type is: ";
                type->dump();

                return NULL; //same as above, yields type casted to
            }
            case llvm::Instruction::Call:
            {
                std::cerr << "** Backtracked to a function call: ";
                backtrack->dump();

                //get return type
                type = backtrack->getType();
                std::cerr << "*** Original (return) types is: ";
                type->dump();

                return type;
            }
            default:
            {
                std::cerr << "** Backtracked to unknown opcode: " << backtrack->getOpcode()
                          << std::endl;
                return NULL;
            }
            }
        }

        return NULL;
    }

    void OpCountPass::getAnalysisUsage(llvm::AnalysisUsage &Info) const
    {
        //empty for now
    }

    //registration stuff
    char OpCountPass::ID = 0;
    static llvm::RegisterPass<OpCountPass> X("opcount", "IR Profiling pass", false, false);
}
